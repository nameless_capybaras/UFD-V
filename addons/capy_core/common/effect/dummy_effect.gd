extends CapyCore.GameEffect

const DummyEffect = CapyCore.Effects.DummyEffect

var query: String:
	set(new):
		query = new
		batch_update()

var value: Variant:
	set(new):
		value = new
		batch_update()

var scanner_obj: GameObject:
	set(new):
		if scanner_obj is GameObject:
			report(game.objects.remove(scanner_obj), "removing old scanner_obj")

		if new is GameObject:
			report(game.objects.add(new), "ading new scanner_obj")
			report(new.updated.connect(_try_parse), "connecting scanner_obj.updated")

		scanner_obj = new

var game_ref: WeakRef
var game: CapyCore.Game = null:
	set(value):
		game_ref = weakref(value)
	get():
		return game_ref.get_ref()


static func create(
	from_query: String, from_value: Variant, from_game_ref: CapyCore.Game
) -> DummyEffect:
	var new: DummyEffect = DummyEffect.new()
	new.query = from_query
	new.value = from_value
	new.game = from_game_ref
	new.scanner_obj = GameObject.create_standard(new.query, GameEffect.Status.UNTRUSTED)
	return new


func _try_parse() -> void:
	var new_effect: GameEffect = null

	for effect: GameEffect in scanner_obj.effects.get_all():
		new_effect = effect._parse(id, value, status, src)

		if new_effect is GameEffect:
			break

	if new_effect is not GameEffect:
		return

	scanner_obj = null
	report(game.effects.add(new_effect), "adding %s" % new_effect)
	report(game.effects.remove(self), "removing %s" % self)
