extends CapyCore.GameEffect

const LinkEffect = CapyCore.Effects.LinkEffect

var _remote_hooks: Array[Hook] = []


static func from_hooks(hooks: Array[Hook], new_id: String) -> LinkEffect:
	var new_self: LinkEffect = LinkEffect.new()

	new_self._remote_hooks = hooks
	new_self.setup(new_id, Status.TRUSTED, "")

	return new_self


func _get_hooks() -> Array[Hook]:
	return _remote_hooks
