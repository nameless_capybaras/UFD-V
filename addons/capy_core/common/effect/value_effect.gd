extends CapyCore.GameEffect

const ValueEffect = CapyCore.Effects.ValueEffect

var value: Variant


func _init() -> void:
	setup("core.value", Status.TRUSTED, "")


func _get_hooks() -> Array[GameEffect.Hook]:
	return [hook("core.get_value", get_value)]


func _parse(
	new_id: String, new_value: Variant, new_status: GameEffect.Status, new_src: String
) -> GameEffect:
	var new: ValueEffect = ValueEffect.new()

	new.setup(new_id, new_status, new_src)
	new.value = new_value

	return new


func get_value() -> Variant:
	return value
