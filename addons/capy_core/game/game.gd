@tool
extends CapyCore.Internals.Base

const ID_SEPARATOR: String = CapyLib.ID_SEPARATOR
const QUERY_SEPARATOR: String = CapyLib.EXTENTION_SEPARATOR
const MOD_FILE: String = "mod.json"

const Set = CapyCore.Internals.Set
const GameEffect = CapyCore.GameEffect
const GameObject = CapyCore.GameObject
const DummyEffect = CapyCore.Effects.DummyEffect

var default_mod_status: GameEffect.Status = GameEffect.Status.DISABLED
var mods: Set = batch_make_child(
	Set.create(
		[func(value: Variant) -> bool: return value is String, _mods_pre_add],
		[_mods_pre_remove],
		[_mods_post_add],
		[_mods_post_remove],
	),
	true,
	true
)
var objects: Set = batch_make_child(
	Set.create(
		[func(value: Variant) -> bool: return value is GameObject],
		[],
		[_objects_post_add],
		[_objects_post_remove],
	),
	false,
	true
)
var effects: Set = batch_make_child(
	Set.create(
		[func(value: Variant) -> bool: return value is GameEffect],
		[],
		[_effects_post_add],
		[_effects_post_remove],
	),
	false,
	true
)


func _to_string() -> String:
	return "Game"


#region Effects
func _effects_post_add(effect: GameEffect) -> void:
	report(effect.batch_set_parent(effects), "setting batch_parent of %s" % effect)
	report(
		effect.updated.connect(effects.reload.bind(effect)),
		"connecting %s to effects.reload" % effect
	)
	var fn: Callable = func() -> void:
		for object: GameObject in objects.get_all():
			report(object.effects.add(effect), "adding %s", [OK, ERR_INVALID_PARAMETER])

	batch_run(fn)


func _effects_post_remove(effect: GameEffect) -> void:
	report(effect.batch_set_parent(null), "setting null batch_parent to effect")

	var reload_fn: Callable = effects.reload.bind(effect)
	if effect.updated.is_connected(reload_fn):
		effect.updated.disconnect(reload_fn)
	else:
		report(false, "checking if effect.updated is connected in _post_remove (%s)" % effect)

	for object: GameObject in objects.get_all():
		report(
			object.effects.remove(effect),
			"removing %s from %s" % [effect, object],
			[OK, ERR_DOES_NOT_EXIST]
		)


#endregion


#region Mods
func _mods_pre_add(path: String) -> Error:
	if not DirAccess.dir_exists_absolute(path):
		return ERR_DOES_NOT_EXIST

	for mod: String in mods.get_all():
		if mod.begins_with(path) or path.begins_with(mod):
			return ERR_ALREADY_EXISTS

	return OK


func _mods_pre_remove(path: String) -> Error:
	var erased_mods: Array[String] = []

	for mod: String in mods.get_all():
		if mod.begins_with(path):  # mod: a/b/c path: a/b
			erased_mods.append(mod)
			continue

		if path.begins_with(mod):  # mod: a/b path: a/b/c
			return ERR_FILE_BAD_PATH

	if erased_mods.is_empty():
		return ERR_DOES_NOT_EXIST

	if erased_mods == [path]:
		return OK

	var remove_mods: Callable = func() -> void:
		for mod: String in erased_mods:
			report(mods.remove(mod), "removing mod %s" % mod)

	batch_run(remove_mods)

	return OK


func _mods_post_add(path: String) -> void:
	var rec_fn: Callable = func(dir: String) -> Array[Array]:
		var subcalls: Array[Array] = []

		for subdir: String in DirAccess.get_directories_at(dir):
			subcalls.append([dir.path_join(subdir)])

		var json: Dictionary = CapyLib.unfold_dict(CapyLib.open_json(dir.path_join(MOD_FILE)))

		for key: String in json.keys():
			var splitted_key: Array[String] = CapyLib.split(key, QUERY_SEPARATOR, 1)
			var id: String = splitted_key[0].strip_edges()

			var query: String = "core.value"
			if splitted_key.size() == 2:
				query = splitted_key[1].strip_edges()

			var new_dummy: DummyEffect = DummyEffect.create(query, json[key], self)
			new_dummy.setup(id, default_mod_status, dir)
			report(effects.add(new_dummy), "adding %s" % new_dummy)

		return subcalls

	batch_run(CapyLib.rec_fn.bindv([[path], rec_fn]))


func _mods_post_remove(path: String) -> void:
	batch_run(
		func() -> void:
			var removed_effects: Array[GameEffect] = effects.get_all().filter(
				func(effect: GameEffect) -> bool: return effect.src.begins_with(path)
			)

			for effect: GameEffect in removed_effects:
				report(effects.remove(effect), "removing %s while removing %s" % [effect, path])
	)


#endregion


#region Objects
func _objects_post_add(object: GameObject) -> void:
	report(object.batch_set_parent(objects), "setting batch_parent to %s" % object)
	report(object.effects.add_set(effects), "adding Game effects to %s" % object)


func _objects_post_remove(object: GameObject) -> void:
	report(object.effects.remove_set(effects), "removing all effects from %s" % object)
	report(object.batch_set_parent(null), "setting null batch_parent to %s" % object)

#endregion
