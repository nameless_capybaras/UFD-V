@tool
class_name CapyLib extends RefCounted

const ID_SEPARATOR: String = "."
const EXTENTION_SEPARATOR: String = ":"


static func split(
	path: String, separator: String = ID_SEPARATOR, max_splits: int = 0
) -> Array[String]:
	return Array(Array(path.split(separator, false, max_splits)), TYPE_STRING, "", null)


static func join(path: Array[String], separator: String = ID_SEPARATOR) -> String:
	return separator.join(path.filter(func(i: String) -> bool: return not i.is_empty()))


static func fold_dict(dictionary: Dictionary, separator: String = ID_SEPARATOR) -> Dictionary:
	var result: Dictionary = {}
	var scream: Callable = func(message: String) -> void:
		push_error("Cannot fold Dictionary: " + message)

	for key: String in dictionary.keys():
		var current_dict: Dictionary = result

		var key_path: Array[String] = split(key, separator)

		if key_path.is_empty():
			push_warning("While folding Dictionary: key is probably malformed: %s" % key)
			continue

		var key_last: String = key_path.pop_back()

		for key_part: String in key_path:
			if current_dict.get_or_add(key_part, {}) is not Dictionary:
				scream.call("Property is defined twice: " + key)
				return {}

			current_dict = current_dict.get(key_part)

		if current_dict.has(key_last):
			scream.call("Property is defined twice: " + key)
			return {}

		current_dict[key_last] = dictionary[key]

	return result


# filter: Callable(String) -> bool
static func unfold_dict(
	dictionary: Dictionary,
	separator: String = ID_SEPARATOR,
	filter: Callable = func(key: String) -> bool: return not key.contains(EXTENTION_SEPARATOR)
) -> Dictionary:
	var result: Dictionary = {}

	var fn: Callable = func(current_dict: Dictionary, current_path: String) -> Array[Array]:
		var return_calls: Array[Array] = []

		for key: String in current_dict.keys():
			var value: Variant = current_dict[key]
			var new_path: String = join([current_path, key], separator)

			if not filter.call(new_path):
				result[new_path] = value
				continue

			if value is Dictionary:
				return_calls.append([value, new_path])
				continue

			if result.has(new_path):
				push_error("Cannot unfold: parameter %s is defined twice" % new_path)
				return []

			result[new_path] = value

		return return_calls

	rec_fn([dictionary, ""], fn)

	return result


static func open_json(path: String, debug_name: String = "[generic data]") -> Dictionary:
	var scream: Callable = func(message: String) -> void:
		push_error("Cannot open JSON file (as %s) at %s: %s" % [debug_name, path, message])

	var file: FileAccess = FileAccess.open(path, FileAccess.READ)
	if not file:
		var load_report: Error = FileAccess.get_open_error()
		if load_report == ERR_FILE_NOT_FOUND:
			return {}

		scream.call("File access error: %s" % error_string(load_report))
		return {}

	var json: JSON = JSON.new()
	var error: Error = json.parse(file.get_as_text())
	file.close()

	if error != OK:
		scream.call(
			"JSON parse error at line %s: %s" % [json.get_error_line(), json.get_error_message()]
		)
		return {}

	if json.data is not Dictionary:
		scream.call("The JSON structure is not a Dictionary/Object")
		return {}

	var data: Dictionary = json.data
	return data


static func save_json(
	path: String, data: Dictionary, debug_name: String = "[generic data]"
) -> Error:
	var scream: Callable = func(message: String) -> void:
		push_error("Cannot save JSON file (as %s) at %s: %s" % [debug_name, path, message])

	var data_json: String = JSON.stringify(data, "\t")

	var file: FileAccess = FileAccess.open(path, FileAccess.WRITE)
	if not file:
		var error: Error = FileAccess.get_open_error()
		scream.call("File access error: %s" % error_string(error))
		return error

	file.store_string(data_json)
	file.close()

	return OK


static func rec_fn(starting_args: Array, fn: Callable) -> void:
	var rec_func: Callable = func(rec_self: Callable, args: Array) -> void:
		var results: Array[Array] = fn.callv(args)

		for arguments: Array in results:
			rec_self.call(rec_self, arguments)

	rec_func.call(rec_func, starting_args)


static func print_section(message_start: String, fn: Callable, message_end: String = "") -> Variant:
	var max_len: int = max(message_start.length(), message_end.length())

	var decorate: Callable = func(msg: String) -> String:
		var length_diff: int = max_len - msg.length()

		var separator_length: int = floor(length_diff / 2.0) + 5
		var l_separator: String = "─".repeat(separator_length)
		var r_separator: String = "─".repeat(separator_length + (length_diff % 2))

		var joiner: String = " " if msg.length() != 0 else "─"

		return joiner.join([l_separator, msg, r_separator])

	print("\n" + decorate.call(message_start))

	var result: Variant = fn.call()

	print(decorate.call(message_end) + "\n")

	return result


static func print_tree(tree: Dictionary, root_name: String, indent: String = "│ ") -> void:
	var fn: Callable = func(
		branch_name: String, branch: Dictionary, indent_count: int
	) -> Array[Array]:
		print(indent.repeat(indent_count) + branch_name)

		var calls: Array[Array] = []

		for key: String in branch.keys():
			var value: Variant = branch[key]

			if value is Dictionary:
				calls.append([key, value, indent_count + 1])
				continue

			print(indent.repeat(indent_count + 1), key)

		return calls
	rec_fn([root_name, tree, 0], fn)
