@tool
extends EditorPlugin


func _enable_plugin() -> void:
	print("Plugin added !")


func _disable_plugin() -> void:
	print("Plugin removed :(")


func plugin_path(suffix: String) -> String:
	var script: Script = get_script()
	return script.get_path().get_base_dir().path_join(suffix)
