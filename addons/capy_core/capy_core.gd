class_name CapyCore extends RefCounted

const PATH: String = "res://addons/capy_core/"  # remeber the trailing /

const GameEffect = preload(PATH + "effect/effect.gd")
const GameObject = preload(PATH + "object/object.gd")
const Game = preload(PATH + "game/game.gd")


class Internals:
	extends RefCounted
	const SUB_PATH: String = PATH + "internals/"
	const Set = preload(SUB_PATH + "set/set.gd")
	const Base = preload(SUB_PATH + "base/base.gd")
	const Report = preload(SUB_PATH + "report/report.gd")


class Effects:
	extends RefCounted
	const SUB_PATH: String = PATH + "common/effect/"
	const DummyEffect = preload(SUB_PATH + "dummy_effect.gd")
	const ScriptEffect = preload(SUB_PATH + "script_effect.gd")
	const ValueEffect = preload(SUB_PATH + "value_effect.gd")
	const LinkEffect = preload(SUB_PATH + "link_effect.gd")
