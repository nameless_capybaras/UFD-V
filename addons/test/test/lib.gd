extends Object

var tests: Array[Array] = [
	[rec_fn_fibonacci, [0, 1, 1, 2, 3, 5, 8, 13, 21]],
	[split, ["around", "the", "world"]],
	[join, "around.the.world"],
	[fold_dict, true],
	[unfold_dict, true],
]


func _test() -> bool:
	for test: Array in tests:
		var test_fn: Callable = test[0]
		var expected: Variant = test[1]
		var result: Variant = test_fn.call()

		if result != test[1]:
			push_error("%s failed: expected %s, got %s" % [test_fn, expected, result])
			return false

	return true


func rec_fn_fibonacci() -> Array:
	var sequence: Array[int] = [0, 1]

	CapyLib.rec_fn(
		[0, 1],
		func(first: int, second: int) -> Array[Array]:
			var next: int = first + second
			sequence.append(next)

			if next >= 21:
				return []

			return [[second, next]]
	)

	return sequence


func split() -> Array:
	return CapyLib.split("around...the...world")


func join() -> String:
	return CapyLib.join(["around", "", "the", "", "world"])


func fold_dict() -> bool:
	var result: Dictionary = CapyLib.fold_dict({"around.the": 1, "around.world": 3})
	var expected: Dictionary = {"around": {"the": 1, "world": 3}}

	if not result.recursive_equal(expected, 3):
		push_error("fold_dict failed: %s" % result)
		return false

	return true


func unfold_dict() -> bool:
	var result: Dictionary = CapyLib.unfold_dict({"around": {"the": 1, "world": 3}})
	var expected: Dictionary = {"around.the": 1, "around.world": 3}

	if not result.recursive_equal(expected, 3):
		push_error("unfold_dict failed: %s" % result)
		return false

	return true
