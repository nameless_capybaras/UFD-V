@tool
extends Control


func _ready() -> void:
	($Container/Actions/Add as Button).icon = get_editor_icon("Add")
	($Container/Actions/Search as LineEdit).right_icon = get_editor_icon("Search")


func get_editor_icon(icon: String, category: String = "EditorIcons") -> Texture2D:
	return EditorInterface.get_editor_theme().get_icon(icon, category)
