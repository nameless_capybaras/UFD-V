@tool
extends Control


func _ready() -> void:
	($Container/Actions/Add as Button).icon = get_editor_icon("Add")
	($Container/Actions/Search as LineEdit).right_icon = get_editor_icon("Search")
	($Container/Actions/Options as MenuButton).icon = get_theme_icon("GuiTabMenuHl", "EditorIcons")
	reload_mods()


func get_editor_icon(icon: String, category: String = "EditorIcons") -> Texture2D:
	return EditorInterface.get_editor_theme().get_icon(icon, category)


func reload_mods() -> void:
	var tree: Tree = $Container/ModTree
	tree.clear()

	var root: TreeItem = tree.create_item()
	root.set_text(0, "root")

	# for id: String in Game.mods.get_all_mods():
	# 	var parent: TreeItem = root

	# 	for id_part: String in id.split(".", false):
	# 		var child: TreeItem = null

	# 		for new_child: TreeItem in parent.get_children():
	# 			if new_child.get_text(0) == id_part:
	# 				child = new_child
	# 				break

	# 		if child == null:
	# 			child = tree.create_item(parent)
	# 			child.set_text(0, id_part)

	# 		parent = child
