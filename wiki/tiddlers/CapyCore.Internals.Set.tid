created: 20250204161014128
modified: 20250209163723081
tags: Class [[Capy's Core Tools]]
title: CapyCore.Internals.Set
type: text/vnd.tiddlywiki

<$class.intro extends=[[CapyCore.Internals.Base]]>

	//Not get//

	The [[Set|CapyCore.Internals.Set]] class mimics what a `Set` type (a [[Dictionary]] with only keys) could be if [[Godot]] implemented it :(

	It also packs some useful, but nonstandard, functions, to serve as a standard interface to a collection of values with update and filtering capabilities


</$class.intro>


<$class.section title=Signals>

	<$class.item item="added(value: [[Variant]])">

		Emitted when `value` is successfully added to the set

	</$class.item>

	<$class.item item="removed(value: [[Variant]])">

		Emitted when `value` is successfully removed from the set

	</$class.item>

</$class.section>


<$class.section title=Imports tags=folded>

	<$class.simpleitem item="[[Set|CapyCore.Internals.Set]]" />

</$class.section>


<$class.section title=Methods>

	<$class.item item="[[static|Static Functions]] create( ... ) -> [[Set|CapyCore.Internals.Set]]">

		<$class.simpleitem item="pre_add: [[Array]]&#91;PreFn&#93; = []" />

		<$class.simpleitem item="pre_remove: [[Array]]&#91;PreFn&#93; = []" />

		<$class.simpleitem item="post_add: [[Array]]&#91;PostFn&#93; = []" />

		<$class.simpleitem item="post_remove: [[Array]]&#91;PostFn&#93; = []" />

		---

		<$class.simpleitem item="PreFn: [[Callable]]([[Variant]]) -> [[bool]] or [[Error]]" />

		<$class.simpleitem item="PostFn: [[Callable]]([[Variant]]) -> [[void]]" />

		---

		Creates and returns an empty [[Set|CapyCore.Internals.Set]]

		`pre_add/remove` functions are run before adding/removing an element to the set, and by returning `false` or a bad [[Error]] can prevent the addition/removal of the element

		`post_add/remove` functions are run after the addition/removal of an element (by binding to `added()` or `removed()`) and can further process the element

	</$class.item>

	<$class.item item="add(value: [[Variant]]) -> [[Error]]">

		Tries to add `value` and reports the result

	</$class.item>

	<$class.item item="add_set(other: [[Set|CapyCore.Internals.Set]]) -> [[Error]]">

		Tries to add every element on `other`, reports the result of each addition with <code>[[Base|CapyCore.Internals.Base]].report()</code>

		Always returns `OK`

	</$class.item>

	<$class.item item="remove(value: [[Variant]]) -> [[Error]]">

		Tries to remove `value` and reports the result

	</$class.item>

	<$class.item item="remove_set(other: [[Set|CapyCore.Internals.Set]]) -> [[Error]]">

		Tries to remove every element on `other`, reports the result of each removal with <code>[[Base|CapyCore.Internals.Base]].report()</code>

		Always returns `OK`

	</$class.item>

	<$class.item item="reload(value: [[Variant]]) -> [[Error]]">

		`add()`s and then `remove()`s `value`

		If any [[Error]] occurs, the process is stopped and the [[Error]] is returned

	</$class.item>

	<$class.item item="has(value: [[Variant]]) -> [[bool]]">

		Checks if `value` is present

	</$class.item>

	<$class.item item="get_pre_add() -> [[Array]]&#91;Fn&#93;">

		<$class.simpleitem item="Fn: [[Callable]]([[Variant]]) -> [[Error]] or [[bool]]" />

		---

		Returns all `pre_add` filters

	</$class.item>

	<$class.item item="get_pre_remove() -> [[Array]]&#91;Fn&#93;">

		<$class.simpleitem item="Fn: [[Callable]]([[Variant]]) -> [[Error]] or [[bool]]" />

		---

		Returns all `pre_remove` filters

	</$class.item>

	<$class.item item="get_all() -> [[Array]]">

		Returns all the values within the set

	</$class.item>


	<$class.item item="remove_all() -> [[Error]]">

		Tries to remove every value of the set

		If an [[Error]] occurs, the process is stopped and the [[Error]] is returned

	</$class.item>

</$class.section>


<$class.section title=Internal tags="folded muted">

	<$class.section title=Properties tags=muted>

		<$class.item item="_values: [[Dictionary]]">

			The raw values of the set, in which each value is the `key` and the `value` of the [[Dictionary]]

		</$class.item>

		<$class.item item="_pre_add: [[Array]]&#91;Fn&#93;">

			<$class.simpleitem item="Fn: [[Callable]]([[Variant]]) -> [[bool]] or [[Error]]" />

			---

			A list of functions that are run before adding a value and that can prevent its addition by returning a bad [[Error]] or `false`

		</$class.item>

		<$class.item item="_pre_remove: [[Array]]&#91;Fn&#93;">

			<$class.simpleitem item="Fn: [[Callable]]([[Variant]]) -> [[bool]] or [[Error]]" />

			---

			A list of functions that are run before removing a value and that can prevent its removal by returning a bad [[Error]] or `false`

		</$class.item>

	</$class.section>

	<$class.section title=Methods tags=muted>

		<$class.item item="_check_filter(filter: Fn) -> [[Error]]">

			<$class.simpleitem item="Fn: [[Callable]]() -> [[Error]] or [[bool]]"/>

			---

			Converts a filter's result to an [[Error]]

			If the result is `false`, `ERR_INVALID_PARAMETER` is returned

			If the result is a [[Variant]] `FAILED` is returned

		</$class.item>

	</$class.section>

</$class.section>