created: 20250209180738621
modified: 20250223151931234
tags: [[Capy's Core Tools]] Class CapyCore.GameEffect
title: CapyCore.Effects.DummyEffect
type: text/vnd.tiddlywiki

<$class.intro extends=[[GameEffect|CapyCore.GameEffect]]>

	//Doesn't move very much//

	This effect takes the role of a placeholder and dynamically tries to substitute itself with the correct effect when another effect can parse its `value`

</$class.intro>


<$class.section title=Imports tags=folded>

	<$class.ref ref="CapyCore.Effects.DummyEffect"/>

</$class.section>


<$class.section title=Properties>

	<$class.item item="query: [[String]]">

		The ID query to find [[effects|CapyCore.GameEffect]] eligible for parsing

	</$class.item>

	<$class.item item="value: [[Variant]]">

		The value that will be parsed by eligible effects

	</$class.item>

	<$class.item item="scanner_obj: [[GameObject|CapyCore.GameObject]]">

		The object that holds the effects eligible for parsing and that is responsible to filter them by ID

	</$class.item>

	<$class.item item="game_ref: [[WeakRef]]&#91;[[Game|CapyCore.Game]]&#93;">

		A [[WeakRef]] to `game`

	</$class.item>

	<$class.item item="game: [[Game|CapyCore.Game]]">

		An empty variable that serves as a proxy to `game_ref` via [[set-get|https://docs.godotengine.org/en/stable/tutorials/scripting/gdscript/gdscript_basics.html#properties-setters-and-getters]]

	</$class.item>

</$class.section>


<$class.section title=Methods>

	<$class.item item="[[static|Static Functions]] create( ... ) -> [[DummyEffect|CapyCore.Effects.DummyEffect]]">

		<$class.simpleitem item='from_query: [[String]]'/>

		<$class.simpleitem item='from_value: [[Variant]]'/>

		<$class.simpleitem item='from_game_ref: [[Game|CapyCore.Game]]'/>

---

		Returns a <$class.ref ref="CapyCore.Effects.DummyEffect"/> with its main variables (`query`, `value` and `game`) already set up

	</$class.item>

</$class.section>


<$class.section title=Internal tags="folded muted">

	<$class.section title=Methods tags=muted>

		<$class.item item="_try_parse() -> [[void]]">

			Checks if any effect in `obj_scanner` is able to parse `value`.
			If that is the case, it removes itself and `scanner_obj` from `game`, and adds the parsed effect to `game`

		</$class.item>

	</$class.section>

</$class.section>