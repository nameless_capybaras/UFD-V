@tool
extends RichTextEffect

var bbcode: String = "unskew"


func _process_custom_fx(char_fx: CharFXTransform) -> bool:
	char_fx.transform.y.x = char_fx.env.get("amt", 0.2)
	return true
