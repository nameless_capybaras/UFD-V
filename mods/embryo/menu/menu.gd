extends Node

const Action: GDScript = preload("res://mods/embryo/menu/action/action.gd")

@export var max_ratio: float = 16 / 9.0
@export var actions_container: Control
@export var layers: Array[CanvasLayer]
@onready var menu: Control = $Menu/Main
@onready var aspect_ratio: AspectRatioContainer = $Menu/Main/AspectRatio


func _input(_event: InputEvent) -> void:
	if Input.is_key_pressed(KEY_ESCAPE):
		for layer: CanvasLayer in layers:
			layer.visible = not layer.visible


func _on_quit_pressed(_what: Control) -> void:
	get_tree().quit()


func _on_main_resized() -> void:
	if not menu:
		return

	if not aspect_ratio:
		return

	if menu.size.x / menu.size.y <= max_ratio:
		aspect_ratio.alignment_horizontal = (AspectRatioContainer.ALIGNMENT_BEGIN)
	else:
		aspect_ratio.alignment_horizontal = (AspectRatioContainer.ALIGNMENT_CENTER)


func _on_menu_visibility_changed() -> void:
	if not menu.visible:
		return

	for action_control: Control in actions_container.get_children():
		if action_control is not Action:
			continue

		var action: Action = action_control

		if not action.enabled:
			continue

		action.child_grab_focus()
		break
