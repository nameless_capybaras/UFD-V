extends Node

const GameObject = CapyCore.GameObject
const GameEffect = CapyCore.GameEffect
const ValueEffect = CapyCore.Effects.ValueEffect
const ScriptEffect = CapyCore.Effects.ScriptEffect

var overlay_query: GameObject
var game: CapyCore.Game = CapyCore.Game.new()


# TODO: 0.2.1: Reimplement EmergencyMenu (not needed rn)
func _ready() -> void:
	reload_game()

	overlay_query = GameObject.create_standard(
		"overlay", GameEffect.Status.TRUSTED, "OverlayAPI Object"
	)

	game.report(
		overlay_query.updated.connect(
			overlay_query.call_hook.bindv(["overlay.bind_parent", [self]])
		),
		"connecting overlay_query.updated to hook overlay.bind_parent"
	)

	game.report(game.objects.add(overlay_query), "adding GameObject to query overlays")


func reload_game() -> void:
	var fn: Callable = func() -> void:
		game.report(game.effects.remove_all(), "removing all effects")
		game.report(game.mods.remove_all(), "removing all mods")

		var default_effects: Array[GameEffect] = [ValueEffect.new(), ScriptEffect.new()]

		for default_effect: GameEffect in default_effects:
			game.report(game.effects.add(default_effect), "adding %s" % default_effect)

		game.report(game.mods.add("res://mods"), "adding res://mods")

		for effect: GameEffect in game.effects.get_all():
			if effect.src.begins_with("res://mods"):
				effect.status = GameEffect.Status.TRUSTED

		if not DirAccess.dir_exists_absolute("user://mods"):
			game.report(
				DirAccess.make_dir_recursive_absolute("user://mods"), "creating dir user://mods"
			)

		game.report(game.mods.add("user://mods"), "adding user://mods")

		for effect: GameEffect in game.effects.get_all():
			if effect.src.begins_with("user://mods"):
				effect.status = GameEffect.Status.UNTRUSTED

		print_effects.call()

	CapyLib.print_section("Reloading Game...", game.batch_run.bind(fn), "Game Reloaded")


func print_effects() -> void:
	var print_by_prefix: Callable = func(prefix: String) -> void:
		var effects_dict: Dictionary = {}

		for effect: GameEffect in game.effects.get_all().filter(
			func(fx: GameEffect) -> bool: return fx.src.begins_with(prefix)
		):
			effects_dict[effect.src.trim_prefix(prefix).path_join(effect.id)] = 0

		CapyLib.print_tree(CapyLib.fold_dict(effects_dict, "/"), prefix)

	CapyLib.print_section(
		"Effects",
		func() -> void:
			print_by_prefix.call("res://mods/")
			print("\n")
			print_by_prefix.call("user://mods/")

			print("\nVirtual effects:")
			for effect: GameEffect in game.effects.get_all().filter(
				func(fx: GameEffect) -> bool: return fx.src.is_empty()
			):
				print("│ ", effect.id)
	)
